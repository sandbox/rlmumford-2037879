<?php

/**
 * @file
 * Definition of Drupal\user_anon\UserAnonStorageController
 */

namespace Drupal\user_anon;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\user\UserStorage;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Language\LanguageInterface;

/**
 * Storage controller class to manage (potential anonymous) users.
 */
class UserAnonStorage extends UserStorage {

  /**
   * The table that stores properties, if the entity has multilingual support.
   *
   * @var string
   */
  protected $anonDataTable;

  /**
   * The table that stores properties, if the entity has multilingual support.
   *
   * @var string
   */
  protected $revisionAnonDataTable;

  /**
   * Returns the anonymous data table name.
   *
   * @return string|false
   *   The table name or FALSE if it is not available.
   */
  public function getAnonDataTable() {
    return $this->anonDataTable;
  }

  /**
   * Returns the anonymous revision data table name.
   *
   * @return string|false
   *   The table name or FALSE if it is not available.
   */
  public function getRevisionAnonDataTable() {
    return $this->revisionAnonDataTable;
  }

  /**
   * {@inheritdoc}
   */
  protected function initTableLayout() {
    parent::initTableLayout();

    $this->anonDataTable = 'users_anon_data';
    $this->revisionAnonDataTable = NULL;
    if ($this->entityType->isRevisionable()) {
      $this->revisionAnonDataTable = $this->entityTypeId . '_anon_revision';
    }
  }

  public function getTableMapping(array $storage_definitions = NULL) {
    $table_mapping = $this->tableMapping;

    if (!isset($this->tableMapping) || $storage_definitions) {
      $table_mapping = parent::getTableMapping($storage_definitions);

      $definitions = $storage_definitions ?: $this->entityManager->getFieldStorageDefinitions($this->entityTypeId);
      $definitions = array_filter($definitions, function (FieldStorageDefinitionInterface $definition) use ($table_mapping) {
        return $table_mapping->allowsSharedTableStorage($definition);
      });

      $all_fields = array_keys($definitions);
      $key_fields = array_values(array_filter(array($this->idKey, $this->revisionKey, $this->bundleKey, $this->langcodeKey)));

      // Nodes have all three of these fields, while custom blocks only have
      // log.
      // @todo Provide automatic definitions for revision metadata fields in
      //   https://drupal.org/node/2248983.
      $revision_metadata_fields = array_intersect(array(
        'revision_timestamp',
        'revision_uid',
        'revision_log',
      ), $all_fields);

      $anon_fields = array_keys(array_filter($definitions, function (FieldStorageDefinitionInterface $definition) {
        return $definition->getSetting('user_anon');
      }));
      $anon_fields = array_merge($key_fields, array_diff($anon_fields, $key_fields));

      $revisionable_anon_fields = array_keys(array_filter($definitions, function (FieldStorageDefinitionInterface $definition) {
        return $definition->isRevisionable() && $definition->getSetting('user_anon');
      }));
      $revisionable_anon_fields = array_merge($key_fields, array_diff($revisionable_anon_fields, $key_fields));

      $revisionable = $this->entityType->isRevisionable();
      $translatable = $this->entityType->isTranslatable();
      if (!$revisionable && $translatable) {
        $table_mapping
          ->setFieldNames($this->anonDataTable, array_values($anon_fields))
          ->setExtraColumns($this->anonDataTable, array('default_langcode'));
      }
      elseif ($revisionable && $translatable) {
        $table_mapping
          ->setFieldNames($this->anonDataTable, $anon_fields)
          // Add the denormalized 'default_langcode' field to the mapping. Its
          // value is identical to the query expression
          // "base_langcode = data_table.langcode" where "base_langcode" is
          // the language code of the default revision.
          ->setExtraColumns($this->anonDataTable, array('default_langcode'));

        $revision_data_key_fields = array(
          $this->idKey,
          $this->revisionKey,
          $this->langcodeKey
        );
        $revision_data_fields = array_diff($revisionable_anon_fields, $revision_metadata_fields, array($this->langcodeKey));
        $table_mapping
          ->setFieldNames($this->revisionAnonDataTable, array_merge($revision_data_key_fields, $revision_data_fields))
          ->setExtraColumns($this->revisionAnonDataTable, array('default_langcode'));
      }
    }

    return $table_mapping;
  }

  protected function saveToSharedTables(ContentEntityInterface $entity, $table_name = NULL, $new_revision = NULL) {
    /* @var UserAnon $entity */
    if (!isset($table_name)) {
      $table_name = $this->dataTable;
    }

    if ($table_name == $this->dataTable) {
      parent::saveToSharedTables($entity, $this->anonDataTable, $new_revision);
      if ($entity->isFullUser()) {
        parent::saveToSharedTables($entity, $table_name, $new_revision);
      }
    }
    else {
      parent::saveToSharedTables($entity, $table_name, $new_revision);
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function attachPropertyData(array &$entities) {
    if ($this->dataTable) {
      // If a revision table is available, we need all the properties of the
      // latest revision. Otherwise we fall back to the data table.
      $anon_table = $this->revisionAnonDataTable ?: $this->anonDataTable;
      $table = $this->revisionDataTable ?: $this->dataTable;

      // Build the join condition.
      $join_condition = "anon_data.{$this->idKey} = data.{$this->idKey}";
      if ($this->revisionAnonDataTable) {
        $join_condition .= " AND anon_data.{$this->revisionKey} = data.{$this->revisionKey}";
      }
      if ($this->langcodeKey) {
        $join_condition .= " AND anon_data.{$this->langcodeKey} = data.{$this->langcodeKey}";
      }

      // Work out which fields to coalesce and which not to coalese.
      $table_mapping = $this->getTableMapping();
      $both_tables = array_intersect($table_mapping->getAllColumns($table), $table_mapping->getAllColumns($anon_table));
      $data_only = array_diff($table_mapping->getAllColumns($table), $table_mapping->getAllColumns($anon_table));

      $query = $this->database->select($anon_table, 'anon_data', array('fetch' => \PDO::FETCH_ASSOC));
      $query->leftJoin($table, 'data', $join_condition);
      foreach ($both_tables as $field_name) {
        $query->addExpression("COALESCE(data.{$field_name}, anon_data.{$field_name})", $field_name);
      }
      foreach ($data_only as $field_name) {
        $query->addField('data', $field_name, $field_name);
      }
      $query->condition('anon_data.' . $this->idKey, array_keys($entities))
        ->orderBy('anon_data.' . $this->idKey);

      if ($this->revisionDataTable) {
        // Get the revision IDs.
        $revision_ids = array();
        foreach ($entities as $values) {
          $revision_ids[] = is_object($values) ? $values->getRevisionId() : $values[$this->revisionKey][LanguageInterface::LANGCODE_DEFAULT];
        }
        $query->condition('anon_data.' . $this->revisionKey, $revision_ids);
      }

      $data = $query->execute()->fetchAllAssoc($this->idKey);

      $table_mapping = $this->getTableMapping();
      $translations = array();
      if ($this->revisionDataTable) {
        $data_fields = array_diff($table_mapping->getFieldNames($this->revisionDataTable), $table_mapping->getFieldNames($this->baseTable));
      }
      else {
        $data_fields = $table_mapping->getFieldNames($this->dataTable);
      }

      foreach ($data as $values) {
        $id = $values[$this->idKey];

        // Field values in default language are stored with
        // LanguageInterface::LANGCODE_DEFAULT as key.
        $langcode = empty($values['default_langcode']) ? $values[$this->langcodeKey] : LanguageInterface::LANGCODE_DEFAULT;
        $translations[$id][$langcode] = TRUE;

        foreach ($data_fields as $field_name) {
          $columns = $table_mapping->getColumnNames($field_name);
          // Do not key single-column fields by property name.
          if (count($columns) == 1) {
            $entities[$id][$field_name][$langcode] = $values[reset($columns)];
          }
          else {
            foreach ($columns as $property_name => $column_name) {
              $entities[$id][$field_name][$langcode][$property_name] = $values[$column_name];
            }
          }
        }
      }

      foreach ($entities as $id => $values) {
        // @todo: Try and get this into core.
        if (!isset($translations[$id])) {
          $translations[$id] = array();
        }
        $bundle = $this->bundleKey ? $values[$this->bundleKey][LanguageInterface::LANGCODE_DEFAULT] : FALSE;
        // Turn the record into an entity class.
        $entities[$id] = new $this->entityClass($values, $this->entityTypeId, $bundle, array_keys($translations[$id]));
      }
    }
  }

}
