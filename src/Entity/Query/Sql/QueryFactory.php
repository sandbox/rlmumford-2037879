<?php

/**
 * @file
 * Contains \Drupal\user_anon\Entity\Query\Sql\QueryFactory.
 */

namespace Drupal\user_anon\Entity\Query\Sql;

use Drupal\Core\Entity\Query\Sql\QueryFactory as BaseQueryFactory;

/**
 * Factory class creating query objects for the user_anon storage mechanism.
 */
class QueryFactory extends BaseQueryFactory {
}
