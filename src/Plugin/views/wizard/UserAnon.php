<?php

/**
 * @file
 * Definition of Drupal\user_anon\Plugin\views\wizard\UserAnon.
 */

namespace Drupal\user_anon\Plugin\views\wizard;

use Drupal\views\Plugin\views\wizard\WizardPluginBase;
use Drupal\user\Plugin\views\wizard\Users;
use Drupal\Component\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * Tests creating user views with the wizard.
 *
 * @Plugin(
 *   id = "user_anon",
 *   module = "user_anon",
 *   base_table = "user_anon",
 *   title = @Translation("Users")
 * )
 */
class UserAnon extends WizardPluginBase {

  /**
   * Set the created column.
   */
  protected $createdColumn = 'created';

  /**
   * Set default values for the path field options.
   */
  protected $pathField = array(
    'id' => 'uid',
    'table' => 'user_anon',
    'field' => 'uid',
    'exclude' => TRUE,
    'link_to_user' => FALSE,
    'alter' => array(
      'alter_text' => TRUE,
      'text' => 'user/[uid]'
    )
  );

  /**
   * Overrides Drupal\views\Plugin\views\wizard\WizardPluginBase::defaultDisplayOptions().
   */
  protected function defaultDisplayOptions() {
    $display_options = parent::defaultDisplayOptions();

    // Add permission-based access control.
    $display_options['access']['type'] = 'perm';
    $display_options['access']['perm'] = 'access user profiles';

    // Remove the default fields, since we are customizing them here.
    unset($display_options['fields']);

    /* Field: User: Name */
    $display_options['fields']['label']['id'] = 'label';
    $display_options['fields']['label']['table'] = 'user_anon';
    $display_options['fields']['label']['field'] = 'label';
    $display_options['fields']['label']['provider'] = 'user_anon';
    $display_options['fields']['label']['label'] = '';
    $display_options['fields']['label']['alter']['alter_text'] = 0;
    $display_options['fields']['label']['alter']['make_link'] = 0;
    $display_options['fields']['label']['alter']['absolute'] = 0;
    $display_options['fields']['label']['alter']['trim'] = 0;
    $display_options['fields']['label']['alter']['word_boundary'] = 0;
    $display_options['fields']['label']['alter']['ellipsis'] = 0;
    $display_options['fields']['label']['alter']['strip_tags'] = 0;
    $display_options['fields']['label']['alter']['html'] = 0;
    $display_options['fields']['label']['hide_empty'] = 0;
    $display_options['fields']['label']['empty_zero'] = 0;
    $display_options['fields']['label']['link_to_user'] = 1;
    $display_options['fields']['label']['overwrite_anonymous'] = 0;

    return $display_options;
  }
}
