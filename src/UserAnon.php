<?php

/**
 * @file
 * Definition of Drupal\user_anon\UserAnon
 */

namespace Drupal\user_anon;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\Entity\User;

/**
 * Class to manage the Users whether they are anonymous or not.
 */
class UserAnon extends User {

  /**
   * Determine whether a user is a full user or not.
   *
   * @return bool
   */
  public function isFullUser() {
    return !($this->id() !== 0 && $this->name->getValue());
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields['uid']->setSetting('user_anon', TRUE);
    $fields['langcode']->setSetting('user_anon', TRUE);
    $fields['preferred_langcode']->setSetting('user_anon', TRUE);
    $fields['preferred_admin_langcode']->setSetting('user_anon', TRUE);
    $fields['created']->setSetting('user_anon', TRUE);
    $fields['changed']->setSetting('user_anon', TRUE);
    return $fields;
  }


}
