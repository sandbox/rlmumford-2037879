<?php

/**
 * @file
 * Contains \Drupal\user\UserAnonStorageSchema.
 */

namespace Drupal\user_anon;

use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserStorageSchema;

class UserAnonStorageSchema extends UserStorageSchema {
  
  /**
   * {@inheritdoc}
   */
  protected function getEntitySchemaTables() {
    $tables = parent::getEntitySchemaTables();
    $tables['anon_data_table'] = $this->storage->getAnonDataTable();
    $tables['revision_anon_data_table'] = $this->storage->getRevisionAnonDataTable();
    return $tables;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEntitySchema(ContentEntityTypeInterface $entity_type, $reset = FALSE) {
    $this->checkEntityType($entity_type);
    $entity_type_id = $entity_type->id();

    if (!isset($this->schema[$entity_type_id]) || $reset) {
      // Back up the storage definition and replace it with the passed one.
      // @todo Instead of switching the wrapped entity type, we should be able
      //   to instantiate a new table mapping for each entity type definition.
      //   See https://www.drupal.org/node/2274017.
      $actual_definition = $this->entityManager->getDefinition($entity_type_id);
      $this->storage->setEntityType($entity_type);

      // Prepare basic information about the entity type.
      $tables = $this->getEntitySchemaTables();

      // Initialize the table schema.
      $schema[$tables['base_table']] = $this->initializeBaseTable($entity_type);
      if (isset($tables['revision_table'])) {
        $schema[$tables['revision_table']] = $this->initializeRevisionTable($entity_type);
      }
      if (isset($tables['data_table'])) {
        $schema[$tables['data_table']] = $this->initializeDataTable($entity_type);
      }
      if (isset($tables['anon_data_table'])) {
        $schema[$tables['anon_data_table']] = $this->initializeDataTable($entity_type);
      }
      if (isset($tables['revision_data_table'])) {
        $schema[$tables['revision_data_table']] = $this->initializeRevisionDataTable($entity_type);
      }
      if (isset($tables['revision_anon_data_table'])) {
        $schema[$tables['revision_anon_data_table']] = $this->initializeRevisionDataTable($entity_type);
      }

      // We need to act only on shared entity schema tables.
      $table_mapping = $this->storage->getTableMapping();
      $table_names = array_diff($table_mapping->getTableNames(), $table_mapping->getDedicatedTableNames());
      $storage_definitions = $this->entityManager->getFieldStorageDefinitions($entity_type_id);
      foreach ($table_names as $table_name) {
        if (!isset($schema[$table_name])) {
          $schema[$table_name] = array();
        }
        foreach ($table_mapping->getFieldNames($table_name) as $field_name) {
          if (!isset($storage_definitions[$field_name])) {
            throw new FieldException("Field storage definition for '$field_name' could not be found.");
          }
          // Add the schema for base field definitions.
          elseif ($table_mapping->allowsSharedTableStorage($storage_definitions[$field_name])) {
            $column_names = $table_mapping->getColumnNames($field_name);
            $storage_definition = $storage_definitions[$field_name];
            $schema[$table_name] = array_merge_recursive($schema[$table_name], $this->getSharedTableFieldSchema($storage_definition, $table_name, $column_names));
          }
        }
      }

      // Process tables after having gathered field information.
      $this->processBaseTable($entity_type, $schema[$tables['base_table']]);
      if (isset($tables['revision_table'])) {
        $this->processRevisionTable($entity_type, $schema[$tables['revision_table']]);
      }
      if (isset($tables['data_table'])) {
        $this->processDataTable($entity_type, $schema[$tables['data_table']]);
      }
      if (isset($tables['revision_data_table'])) {
        $this->processRevisionDataTable($entity_type, $schema[$tables['revision_data_table']]);
      }
      if (isset($tables['anon_data_table'])) {
        $this->processDataTable($entity_type, $schema[$tables['anon_data_table']]);
      }
      if (isset($tables['revision_anon_data_table'])) {
        $this->processRevisionDataTable($entity_type, $schema[$tables['revision_anon_data_table']]);
      }

      $this->schema[$entity_type_id] = $schema;

      // Restore the actual definition.
      $this->storage->setEntityType($actual_definition);
    }

    return $this->schema[$entity_type_id];
  }

  /**
   * {@inheritdoc}
   */
  public function onEntityTypeUpdate(EntityTypeInterface $entity_type, EntityTypeInterface $original) {
    $this->checkEntityType($entity_type);
    $this->checkEntityType($original);

    // If no schema changes are needed, we don't need to do anything.
    //if (!$this->requiresEntityStorageSchemaChanges($entity_type, $original)) {
    //  return;
    //}

    dpm($entity_type->getStorageClass(), $original->getStorageClass());
    if ($entity_type->getStorageClass() == 'Drupal\user_anon\UserAnonStorage'
      && $original->getStorageClass() == 'Drupal\user\UserStorage') {
      $this->addAnonTables($entity_type);
    }
    else {
      parent::onEntityTypeUpdate($entity_type, $original);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function requiresEntityDataMigration(EntityTypeInterface $entity_type, EntityTypeInterface $original)  {
    $original_storage_class = $original->getStorageClass();
    if (in_array($original_storage_class, array('Drupal\user\UserStorage', 'Drupal\user\UserAnonStorage'))) {
      return FALSE;
    }

    return parent::requiresEntityDataMigration($entity_type, $original);
  }

  /**
   * {@inheritdoc}
   */
  protected function isTableEmpty($table_name) {
    return TRUE;
  }

  /**
   * Add the anonymous tables.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   */
  protected function addAnonTables(EntityTypeInterface $entity_type) {
    $this->checkEntityType($entity_type);
    $schema_handler = $this->database->schema();

    // Create entity tables.
    $schema = $this->getEntitySchema($entity_type, TRUE);
    foreach ($schema as $table_name => $table_schema) {
      if (!$schema_handler->tableExists($table_name)) {
        $schema_handler->createTable($table_name, $table_schema);
      }
    }

    // @todo Make this dynamically calculate which fields to collect.
    $selectQuery = $this->database->select('users_field_data', 'fd')
      ->fields('fd', array('uid', 'langcode', 'preferred_langcode', 'preferred_admin_langcode', 'created', 'changed', 'default_langcode'));
    $this->database->insert('users_anon_data')->from($selectQuery)->execute();

    // Save data about entity indexes and keys.
    $this->saveEntitySchemaData($entity_type, $schema);
  }
}